package com.example.mohammedmobinmunir.dubizzlesample.helpers;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by MohammedMobinMunir on 11/27/2017.
 */

public class SleekUI {


    public static void initRecyclerView(RecyclerView recyclerView, ListStyle listStyle) {
        int i;
        if (listStyle.equals(ListStyle.vertical))
            i = LinearLayoutManager.VERTICAL;
        else
            i = LinearLayoutManager.HORIZONTAL;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), i, false));
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(20);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);


    }


    public enum ListStyle {
        vertical, horizontal


    }
}
