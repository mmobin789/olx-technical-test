package com.example.mohammedmobinmunir.dubizzlesample.models;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by MohammedMobinMunir on 11/27/2017.
 */

public class User implements Parcelable {
    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
    private String username;
    private long id;

    public User(String username, long id) {
        this.username = username;
        this.id = id;
    }

    private User(Parcel in) {
        this.username = in.readString();
        this.id = in.readLong();
    }

    public String getUserName() {
        return username;
    }

    public long getId() {
        return id;
    }

    @Override
    public String toString() {
        return "username " + username + " id " + id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.username);
        dest.writeLong(this.id);
    }
}
