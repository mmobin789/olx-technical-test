package com.example.mohammedmobinmunir.dubizzlesample.subcontrollers;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mohammedmobinmunir.dubizzlesample.models.TweetQuery;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AndroidFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AndroidFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match

    private int pageNo;

    public AndroidFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment AndroidFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AndroidFragment newInstance() {
        AndroidFragment fragment = new AndroidFragment();
        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    int getAutoPageNo() {
        pageNo++;
        return pageNo;
    }

    @Override
    TweetQuery hitTweetsAPI() {
        TweetQuery tweetQuery = new TweetQuery();
        tweetQuery.setScreenName("AndroidDev");
        tweetQuery.setPage(getAutoPageNo());
        tweetQuery.setCount(20);
        return tweetQuery;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}
