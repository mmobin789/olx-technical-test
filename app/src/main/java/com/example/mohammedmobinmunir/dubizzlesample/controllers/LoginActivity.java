package com.example.mohammedmobinmunir.dubizzlesample.controllers;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.example.mohammedmobinmunir.dubizzlesample.R;
import com.example.mohammedmobinmunir.dubizzlesample.api.ApiCall;
import com.example.mohammedmobinmunir.dubizzlesample.interfaces.OnLoginListener;
import com.example.mohammedmobinmunir.dubizzlesample.models.Auth;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;

@RuntimePermissions
public class LoginActivity extends BaseActivity implements OnLoginListener {
    TwitterLoginButton button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Twitter.initialize(this);
        setContentView(R.layout.activity_login);
        super.onCreate(savedInstanceState);

    }

    @Override
    void initializeViews() {
        button = (TwitterLoginButton) findViewById(R.id.login_button);
        initTwitter();
    }

    @NeedsPermission(Manifest.permission.INTERNET)
    void initTwitter() {
        button.setCallback(ApiCall.twitterLogin(this));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        button.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void OnLoginSuccess(Auth auth) {

        Intent main = new Intent(this, MainActivity.class);
        main.putExtra("user", auth);
        startActivity(main);
    }

    @Override
    public void OnLoginFailed(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }
}
