package com.example.mohammedmobinmunir.dubizzlesample.interfaces;

/**
 * Created by MohammedMobinMunir on 12/4/2017.
 */

public interface OnTweetClickListener {
    void OnTweetClicked(int position);
}
