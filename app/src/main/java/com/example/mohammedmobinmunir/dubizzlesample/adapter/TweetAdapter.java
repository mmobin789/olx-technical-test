package com.example.mohammedmobinmunir.dubizzlesample.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mohammedmobinmunir.dubizzlesample.R;
import com.example.mohammedmobinmunir.dubizzlesample.interfaces.OnTweetClickListener;
import com.squareup.picasso.Picasso;

import java.util.List;

import twitter4j.Status;

/**
 * Created by MohammedMobinMunir on 12/2/2017.
 */

public class TweetAdapter extends RecyclerView.Adapter<TweetAdapter.VH> {
    private List<Status> list;
    private OnTweetClickListener listener;

    public TweetAdapter(List<Status> list) {
        this.list = list;
    }

    public void setOnTweetClickListener(OnTweetClickListener listener) {
        this.listener = listener;
    }

    @Override
    public VH onCreateViewHolder(ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_layout_tweets, parent, false));
    }

    @Override
    public void onBindViewHolder(VH holder, int position) {
        Status status = list.get(position);
        holder.tvName.setText(status.getUser().getName());
        holder.tvUsername.setText(status.getUser().getScreenName());
        holder.tvTweet.setText(status.getText());
        holder.tvDate.setText(String.valueOf(status.getCreatedAt()));
        Picasso.with(holder.itemView.getContext()).load(status.getUser().getOriginalProfileImageURL()).into(holder.iv);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class VH extends RecyclerView.ViewHolder {
        TextView tvName, tvUsername, tvTweet, tvDate;
        ImageView iv;

        VH(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.name);
            tvUsername = itemView.findViewById(R.id.screenName);
            tvTweet = itemView.findViewById(R.id.tweet);
            tvDate = itemView.findViewById(R.id.date);
            iv = itemView.findViewById(R.id.userPic);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null)
                        listener.OnTweetClicked(getAdapterPosition());
                }
            });
        }
    }
}

