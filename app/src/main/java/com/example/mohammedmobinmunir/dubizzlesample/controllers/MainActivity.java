package com.example.mohammedmobinmunir.dubizzlesample.controllers;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.example.mohammedmobinmunir.dubizzlesample.R;
import com.example.mohammedmobinmunir.dubizzlesample.adapter.PagerAdapter;
import com.example.mohammedmobinmunir.dubizzlesample.helpers.LocaleHelper;
import com.example.mohammedmobinmunir.dubizzlesample.models.Auth;
import com.example.mohammedmobinmunir.dubizzlesample.subcontrollers.AndroidFragment;
import com.example.mohammedmobinmunir.dubizzlesample.subcontrollers.OlxEgyptFragment;
import com.example.mohammedmobinmunir.dubizzlesample.subcontrollers.UserFragment;
import com.twitter.sdk.android.core.TwitterCore;

import java.util.Locale;

public class MainActivity extends BaseActivity implements View.OnClickListener {

    TextView tab1, tab2, tab3;
    ViewPager viewPager;
    ImageView switchLocale, logOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_main);
        super.onCreate(savedInstanceState);

    }


    @Override
    void initializeViews() {
        switchLocale = (ImageView) findViewById(R.id.switchLocale);
        logOut = (ImageView) findViewById(R.id.logOut);
        viewPager = (ViewPager) findViewById(R.id.pager);
        Auth auth = getIntent().getParcelableExtra("user");
        PagerAdapter pagerAdapter = new PagerAdapter(getSupportFragmentManager());
        pagerAdapter.addFragment(UserFragment.newInstance(auth));
        pagerAdapter.addFragment(OlxEgyptFragment.newInstance());
        pagerAdapter.addFragment(AndroidFragment.newInstance());
        viewPager.setAdapter(pagerAdapter);
        tab1 = (TextView) findViewById(R.id.tab1);
        tab2 = (TextView) findViewById(R.id.tab2);
        tab3 = (TextView) findViewById(R.id.tab3);
        switchLocale.setOnClickListener(this);
        tab1.setOnClickListener(this);
        tab2.setOnClickListener(this);
        tab3.setOnClickListener(this);
        logOut.setOnClickListener(this);
        tab1.performClick();


    }

    private void switchLocale() {
        String lang = Locale.getDefault().getLanguage();
        if (lang.equalsIgnoreCase("en"))
            updateUI("ar");
        else
            updateUI("en");
        recreate();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tab1:
                viewPager.setCurrentItem(0);
                break;
            case R.id.tab2:
                viewPager.setCurrentItem(1);
                break;
            case R.id.tab3:
                viewPager.setCurrentItem(2);
                break;
            case R.id.switchLocale:
                switchLocale();
                break;
            case R.id.logOut:
                showLogOutMenu(view);
                break;

        }
    }

//    public static boolean anagram(String s1, String s2) {
//        if (s1.length() != s2.length()) return false;
//
//        s1 = s1.toLowerCase();
//        s2 = s2.toLowerCase();
//
//        int sum = 0;
//        for (int i = 0; i < s1.length(); i++) {
//            sum += s1.charAt(i) - s2.charAt(i);
//        }
//
//        return sum == 0;
//
//    }

    private void showLogOutMenu(View v) {
        PopupMenu popup;
        popup = new PopupMenu(v.getContext(), v, Gravity.START);
        popup.getMenuInflater().inflate(R.menu.logout, popup.getMenu());
        popup.show();
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                TwitterCore.getInstance().getSessionManager().clearActiveSession();
                onBackPressed();
                return true;
            }
        });
    }

    @Override
    public void recreate() {
        finish();
        overridePendingTransition(0, 0);
        startActivity(getIntent());
        overridePendingTransition(0, 0);
    }

    private void updateUI(String language) {
        Resources res = LocaleHelper.setLocale(this, language).getResources();
        tab1.setText(res.getString(R.string.user));
        tab2.setText(res.getString(R.string.olxEgypt));
        tab3.setText(res.getString(R.string.androidDev));

    }
}
