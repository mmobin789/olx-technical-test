package com.example.mohammedmobinmunir.dubizzlesample.interfaces;

import com.example.mohammedmobinmunir.dubizzlesample.models.Auth;

/**
 * Created by MohammedMobinMunir on 11/27/2017.
 */

public interface OnLoginListener {
    void OnLoginSuccess(Auth auth);

    void OnLoginFailed(String error);
}
