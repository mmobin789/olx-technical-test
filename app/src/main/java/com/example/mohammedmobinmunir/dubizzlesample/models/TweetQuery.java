package com.example.mohammedmobinmunir.dubizzlesample.models;

/**
 * Created by MohammedMobinMunir on 12/2/2017.
 */

public class TweetQuery {
    private int page, count;
    private String screenName;


    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getScreenName() {
        return screenName;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }
}
