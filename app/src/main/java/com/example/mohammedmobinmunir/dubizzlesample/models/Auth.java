package com.example.mohammedmobinmunir.dubizzlesample.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by MohammedMobinMunir on 11/27/2017.
 */

public class Auth implements Parcelable {
    public static final Parcelable.Creator<Auth> CREATOR = new Parcelable.Creator<Auth>() {
        @Override
        public Auth createFromParcel(Parcel source) {
            return new Auth(source);
        }

        @Override
        public Auth[] newArray(int size) {
            return new Auth[size];
        }
    };
    private String token, secret;
    private boolean isExpired;
    private User user;

    public Auth(String token, String secret) {
        this.token = token;
        this.secret = secret;

    }

    private Auth(Parcel in) {
        this.secret = in.readString();
        this.token = in.readString();
        this.isExpired = in.readByte() != 0;
        this.user = in.readParcelable(User.class.getClassLoader());
    }

    public String getToken() {
        return token;
    }

    public String getSecret() {
        return secret;
    }

    public boolean isExpired() {
        return isExpired;
    }

    public void setExpired(boolean expired) {
        isExpired = expired;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "token " + token + " secret " + secret + " " + user;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.secret);
        dest.writeString(this.token);
        dest.writeByte(this.isExpired ? (byte) 1 : (byte) 0);
        dest.writeParcelable(this.user, flags);
    }
}
