package com.example.mohammedmobinmunir.dubizzlesample.interfaces;

import java.util.List;

import twitter4j.Status;

/**
 * Created by MohammedMobinMunir on 12/3/2017.
 */

public interface OnTweetsListener {
    void OnTweetsFetched(List<Status> tweets);

    void OnAllTweetsLoaded(boolean allLoaded);

    void OnTweetsError(String error);
}
