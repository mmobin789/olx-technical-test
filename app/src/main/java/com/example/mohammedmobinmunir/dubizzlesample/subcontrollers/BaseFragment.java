package com.example.mohammedmobinmunir.dubizzlesample.subcontrollers;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mohammedmobinmunir.dubizzlesample.R;
import com.example.mohammedmobinmunir.dubizzlesample.adapter.TweetAdapter;
import com.example.mohammedmobinmunir.dubizzlesample.api.ApiCall;
import com.example.mohammedmobinmunir.dubizzlesample.helpers.SleekUI;
import com.example.mohammedmobinmunir.dubizzlesample.interfaces.OnTweetClickListener;
import com.example.mohammedmobinmunir.dubizzlesample.interfaces.OnTweetsListener;
import com.example.mohammedmobinmunir.dubizzlesample.models.TweetQuery;
import com.paginate.Paginate;

import java.util.ArrayList;
import java.util.List;

import twitter4j.Status;

/**
 * Created by MohammedMobinMunir on 12/4/2017.
 */

public abstract class BaseFragment extends Fragment implements OnTweetsListener, OnTweetClickListener {
    protected RecyclerView recyclerView;
    protected TweetAdapter adapter;
    protected List<Status> list = new ArrayList<>();
    protected boolean allTweetsLoaded;
    protected Paginate.Callbacks callbacks = new Paginate.Callbacks() {
        @Override
        public void onLoadMore() {
            hitApi();
        }

        @Override
        public boolean isLoading() {
            return allTweetsLoaded;
        }

        @Override
        public boolean hasLoadedAllItems() {
            return allTweetsLoaded;
        }
    };

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        recyclerView = view.findViewById(R.id.rv);
        SleekUI.initRecyclerView(recyclerView, SleekUI.ListStyle.vertical);
        adapter = new TweetAdapter(list);
        recyclerView.setAdapter(adapter);
        adapter.setOnTweetClickListener(this);
        Paginate.with(recyclerView, callbacks).addLoadingListItem(true).build();

    }

    private void hitApi() {
        ApiCall.init().getTweets(getContext(), hitTweetsAPI(), this);
    }

    private void showTweetInfoDialog(int position) {
        Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.adapter_layout_tweets);
        TextView tvName = dialog.findViewById(R.id.name);
        TextView tvUsername = dialog.findViewById(R.id.screenName);
        TextView tvTweet = dialog.findViewById(R.id.tweet);
        TextView tvDate = dialog.findViewById(R.id.date);
        ImageView iv = dialog.findViewById(R.id.userPic);
        Status status = list.get(position);
        String data = getString(R.string.tweet) + " " + status.getCreatedAt() + " "
                + getString(R.string.fav) + " " + status.getFavoriteCount() + " " +
                getString(R.string.retweet) + " " + status.getRetweetCount();
        tvTweet.setText(data);
        iv.setVisibility(View.GONE);
        tvName.setVisibility(View.GONE);
        tvUsername.setVisibility(View.GONE);
        tvDate.setVisibility(View.GONE);
        dialog.show();


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment, container, false);
    }

    abstract TweetQuery hitTweetsAPI();

    abstract int getAutoPageNo();

    @Override
    public void OnTweetsFetched(List<Status> tweets) {
        list.addAll(tweets);
        adapter.notifyItemRangeInserted(adapter.getItemCount(), tweets.size());
    }

    @Override
    public void OnAllTweetsLoaded(boolean allLoaded) {
        allTweetsLoaded = allLoaded;
    }

    @Override
    public void OnTweetsError(String error) {
        Log.e("TweetError", error);
    }

    @Override
    public void OnTweetClicked(int position) {
        showTweetInfoDialog(position);
    }
}
