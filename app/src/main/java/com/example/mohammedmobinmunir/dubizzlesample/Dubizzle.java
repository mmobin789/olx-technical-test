package com.example.mohammedmobinmunir.dubizzlesample;

import android.app.Application;
import android.content.Context;

import com.example.mohammedmobinmunir.dubizzlesample.helpers.LocaleHelper;

/**
 * Created by MohammedMobinMunir on 12/3/2017.
 */

public class Dubizzle extends Application {
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base));
    }
}
