package com.example.mohammedmobinmunir.dubizzlesample.api;

import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.example.mohammedmobinmunir.dubizzlesample.R;
import com.example.mohammedmobinmunir.dubizzlesample.interfaces.OnLoginListener;
import com.example.mohammedmobinmunir.dubizzlesample.interfaces.OnTweetsListener;
import com.example.mohammedmobinmunir.dubizzlesample.models.Auth;
import com.example.mohammedmobinmunir.dubizzlesample.models.TweetQuery;
import com.example.mohammedmobinmunir.dubizzlesample.models.User;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;

import java.util.List;

import twitter4j.Paging;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;


/**
 * Created by MohammedMobinMunir on 11/27/2017.
 */

public class ApiCall {
    private static ApiCall apiCall;

    private ApiCall() {
    }

    public static ApiCall init() {
        if (apiCall == null)
            apiCall = new ApiCall();
        return apiCall;
    }

    @NonNull
    public static Callback<TwitterSession> twitterLogin(final OnLoginListener loginListener) {

        return new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                TwitterAuthToken twitterAuthToken = result.data.getAuthToken();
                Auth auth = new Auth(twitterAuthToken.secret, twitterAuthToken.token);
                auth.setExpired(twitterAuthToken.isExpired());
                auth.setUser(new User(result.data.getUserName(), result.data.getUserId()));
                loginListener.OnLoginSuccess(auth);
            }

            @Override
            public void failure(TwitterException exception) {
                loginListener.OnLoginFailed(exception.toString());
            }
        };
    }

    public void getTweets(Context context, TweetQuery query, OnTweetsListener listener) {

        TwitterTask twitterTask = new TwitterTask(context, query, listener);
        twitterTask.execute();


    }

    private class TwitterTask extends AsyncTask<Nullable, Nullable, List<Status>> {
        Context context;
        TweetQuery tweetQuery;
        OnTweetsListener listener;

        TwitterTask(Context context, TweetQuery tweetQuery, OnTweetsListener listener) {
            this.context = context;
            this.listener = listener;
            this.tweetQuery = tweetQuery;
        }


        @Override
        protected List<twitter4j.Status> doInBackground(Nullable... Nullables) {
            ConfigurationBuilder cb = new ConfigurationBuilder();
            cb.setDebugEnabled(true)
                    .setOAuthConsumerKey(context.getString(R.string.com_twitter_sdk_android_CONSUMER_KEY))
                    .setOAuthConsumerSecret(context.getString(R.string.com_twitter_sdk_android_CONSUMER_SECRET))
                    .setOAuthAccessToken(context.getString(R.string.oauth_token))
                    .setOAuthAccessTokenSecret(context.getString(R.string.oauth_token_secret));
            TwitterFactory tf = new TwitterFactory(cb.build());
            Twitter twitter = tf.getInstance();
            Paging paging = new Paging(tweetQuery.getPage(), tweetQuery.getCount());
            List<twitter4j.Status> statusList = null;
            try {
                statusList = twitter.getUserTimeline(tweetQuery.getScreenName(), paging);
                if (statusList.size() == 0)
                    listener.OnAllTweetsLoaded(true);
                else listener.OnAllTweetsLoaded(false);
            } catch (twitter4j.TwitterException e) {
                e.printStackTrace();
                listener.OnTweetsError(e.toString());
            }
            return statusList;
        }

        @Override
        protected void onPostExecute(List<twitter4j.Status> statusList) {

            listener.OnTweetsFetched(statusList);
        }

    }

//    private static class MyRetrofitInterceptor implements Interceptor {
//
//
//        @Override
//        public okhttp3.Response intercept(Chain chain) throws IOException {
//
//            Request.Builder builder = chain.request().newBuilder();
//            builder.header("Authorization", authHeader());
//            builder.header("Content-Type", "application/x-www-form-urlencoded");
//            return chain.proceed(builder.build());
//        }
//    }
}
