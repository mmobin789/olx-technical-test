package com.example.mohammedmobinmunir.dubizzlesample;

import com.example.mohammedmobinmunir.dubizzlesample.models.Auth;
import com.example.mohammedmobinmunir.dubizzlesample.models.TweetQuery;
import com.example.mohammedmobinmunir.dubizzlesample.models.User;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class OLXUnitTest {

    // basic unit testing using mockito framework

    @Test
    public void testAuth() {
        Auth testAuth = mock(Auth.class);
        when(testAuth.isExpired()).thenReturn(false);
        assertEquals(testAuth.isExpired(), false);
    }

    @Test
    public void testUser() {
        User testUser = mock(User.class);
        when(testUser.getId()).thenReturn((long) 22);
        assertEquals(testUser.getId(), 22);
    }

    @Test
    public void testTwitterQuery() {
        TweetQuery query = mock(TweetQuery.class);
        when(query.getCount()).thenReturn(0);
        when(query.getPage()).thenReturn(0);
        assertEquals(query.getCount(), 0);
        assertEquals(query.getPage(), 0);
    }

}